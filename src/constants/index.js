export const OPERATORS = ['+', '-', '*', '/', '**']
export const OPERATORS_COUNT = OPERATORS.length
export const MIN_VALUE_FOR_EXPRESSION = 1
export const MAX_VALUE_FOR_EXPRESSION = 30