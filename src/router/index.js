import {createRouter, createWebHistory} from 'vue-router'
import Settings from '../views/Settings.vue'
import Game from "../views/Game";

const routes = [
    {
        path: '/',
        name: 'Settings',
        props: true,
        component: Settings
    },
    {
        path: '/game',
        name: 'Game',
        props: true,
        component: Game
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
